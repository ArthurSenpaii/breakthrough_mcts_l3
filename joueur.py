import time

from matplotlib.cbook import is_writable_file_like
import interface
import numpy as np
from collections import defaultdict


class Joueur:
	def __init__(self, partie, couleur, opts={}):
		self.couleur = couleur
		self.couleurval = interface.couleur_to_couleurval(couleur)
		self.jeu = partie
		self.opts = opts

	def demande_coup(self):
		pass


class Humain(Joueur):

	def demande_coup(self):
		pass


class IA(Joueur):

	def __init__(self, partie, couleur, opts={}):
		super().__init__(partie, couleur, opts)
		self.temps_exe = 0
		self.nb_appels_jouer = 0


class Random(IA):
	def demande_coup(self):
		coup = self.jeu.plateau.liste_coups_valides(self.couleurval)
		return coup[np.random.randint(len(coup))]


class Minmax(IA):
    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts=opts)
        self.last_time = 0

    def demande_coup(self):
        return self.minmax()

    @staticmethod
    def eval_internal(plateau, couleurval):
        nmb = 0
        for i in range(plateau.taille):
            for j in range(plateau.taille):
                if plateau.tableau_cases[i][j] == 1:
                    nmb += 1
                elif plateau.tableau_cases[i][j] == -1:
                    nmb -= 1
        return couleurval*nmb

    def minmax_internal(self, plateau, couleurval, height=0):
        if height == 0:
            return (Minmax.eval_internal(plateau, self.couleurval), None)
        liste_coups = plateau.liste_coups_valides(couleurval)
        if couleurval == self.couleurval:##maximise
            m, chm = -np.infty, None
            for c in liste_coups:
                newplateau = plateau.copie()
                newplateau.jouer(c, couleurval)
                value, mcoup = self.minmax_internal(
                    newplateau, -couleurval, height-1)
                if value > m:  # on garde le meilleur coup
                    m = value
                    chm = c
        else:#minimise
            m, chm = np.infty, None
            for c in liste_coups:
                newplateau = plateau.copie()
                newplateau.jouer(c, couleurval)
                value, mcoup = self.minmax_internal(
                    newplateau, -couleurval, height-1)
                if value < m:  # on prend le moins bon coup
                    m = value
                    chm = c
        return (m, chm)

    def minmax(self):
        bestgain, bestchemin = self.minmax_internal(
            self.jeu.plateau, self.couleurval, 2)
        return bestchemin


class AlphaBeta(IA):
    def demande_coup(self):
        (m, argm) = self.alphabeta(self.jeu.plateau, self.couleurval, 2, -np.inf, np.inf)
        return argm

    def alphabeta(self, plateau, couleurval, height, alpha, beta):
        if height == 0:
            return (Minmax.eval_internal(plateau, self.couleurval), None)
        liste_coups = plateau.liste_coups_valides(couleurval)
        if couleurval == self.couleurval:##maximise
            m, chm = -np.infty, None
            for c in liste_coups:
                newplateau = plateau.copie()
                newplateau.jouer(c, couleurval)
                value, mcoup = self.alphabeta(
                    newplateau, -couleurval, height-1, -beta, -alpha)
                if value > alpha:  # on garde le meilleur coup
                    alpha = value
                    chm = c
                    if alpha >= beta:
                        break
        else:#minimise
            m, chm = np.infty, None
            for c in liste_coups:
                newplateau = plateau.copie()
                newplateau.jouer(c, couleurval)
                value, mcoup = self.alphabeta(
                    newplateau, -couleurval, height-1, -alpha, -beta)
                if value > alpha:  # on garde le moins bon coup
                    alpha = value
                    chm = c
                    if alpha >= beta:
                        break
        return (m, chm)


class MC(IA):
    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts=opts)
        self.last_time = 0

    def demande_coup(self):
        return self.mc()

    @staticmethod
    def eval_internal(plateau, couleurval):
        nmb = 0
        for i in range(plateau.taille):
            for j in range(plateau.taille):
                if plateau.tableau_cases[i][j] == 1:
                    nmb += 1
                elif plateau.tableau_cases[i][j] == -1:
                    nmb -= 1
        return couleurval*nmb


    def mc_internal(self, plateau, couleurval, MAX=20):
        liste_coups = plateau.liste_coups_valides(couleurval)
        bestprob = 0
        bestcoup = None
        for c in liste_coups:
            win = 0
            for i in range(MAX):
                copie = plateau.copie()
                copie.jouer(c, couleurval)
                actualplayer = -couleurval
                while(True):
                    new_coups = copie.liste_coups_valides(actualplayer)
                    copie.jouer(new_coups[np.random.randint(len(new_coups))], actualplayer)# FIXME liste vide ??
                    if iswin(copie, actualplayer):
                        if(actualplayer == couleurval):
                            win += 1
                            break
                        break;
                    actualplayer = -actualplayer
            if bestprob < (win/MAX):
                bestprob = (win/MAX)
                bestcoup = c
        return (bestprob, bestcoup)
            
                        
    def mc(self):
        (proba,coup) = self.mc_internal(self.jeu.plateau , self.couleurval)
        print("on va jouer {}".format(coup))
        print("avec proba {}".format(proba))
        return coup


class MCTS(IA):
    class Noeud:
        def __init__(self, plateau,couleurval, parent=None, parent_action=None):
            self.plateau = plateau
            self.parent = parent
            self.parent_action = parent_action
            self.enfants = []
            self.couleurval = couleurval
            self._results = defaultdict(int)
            self.win = 0
            self.loses = 0
            self.visits = 0
            return

        def expand(self):
            coup = self.plateau.liste_coups_valides(self.couleurval)[np.random.randint(len(self.plateau.liste_coups_valides(self.couleurval)))]
            newplateau = self.plateau.copie()
            newplateau.jouer(coup, self.couleurval)
            noeud_enfant = MCTS.Noeud(newplateau, -self.couleurval, self, coup)
            self.enfants.append(noeud_enfant)
            return noeud_enfant

        def is_end(self):
            return iswin(self.plateau, self.couleurval) or iswin(self.plateau, -self.couleurval)

        def evaluate(self):
            copie = self.plateau.copie()
            localcolor = self.couleurval

            while not (iswin(copie, self.couleurval) or iswin(copie, -self.couleurval)):
                new_coups = copie.liste_coups_valides(localcolor)
                copie.jouer(new_coups[np.random.randint(len(new_coups))], localcolor) 
                localcolor = -localcolor

            return self.couleurval if iswin(copie, self.couleurval) else -self.couleurval

        def backpropagate(self, result):
            self._results[result] += 1
            self.visits += 1
            if self.parent:
                self.parent.backpropagate(result)

        def is_fully_expanded(self):
            return len(self.enfants) == len(self.plateau.liste_coups_valides(self.couleurval)) # alors le nbr de coup est le meme que le nbr d'enfant


        def best_child(self, k_param = 0.1):
            weight = (self.win / self.visits) + k_param * np.sqrt(np.log(self.visits) / self.visits)  
            print(len(self.enfants))
            return self.enfants[np.argmax(weight)] # FIXME parfois liste vide ??

        def strategie_arbre(self):
            noeud = self
            while not self.is_end():
                if noeud.is_fully_expanded():
                    noeud = noeud.best_child()
                else:
                    return noeud.expand()
            return noeud
        

        def action(self):
            for i in range(400):
                v = self.strategie_arbre()
                reward = v.evaluate()
                v.backpropagate(reward)
            return self.best_child(k_param=0.0)
        
    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts=opts)
        self.last_time = 0

    def demande_coup(self):
        return self.mcts()


    def mcts(self):
        noeud = self.Noeud(self.jeu.plateau, self.couleurval)
        return noeud.action().parent_action



def iswin(plateau, couleurval):
    for i in range(plateau.taille):
        if plateau[i,-couleurval] == couleurval:
            return True
    return False

            
