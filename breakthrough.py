from __future__ import division

import numpy as np

import joueur
import interface


class Jeu:
	
	def __init__(self, opts={}):
		
		self.noir = None
		self.blanc = None
		if "choix_joueurs" not in opts or opts["choix_joueurs"]:
			possibilites = [joueur.Humain] + joueur.IA.__subclasses__()
			if len(possibilites) == 1:
				print("Un seul type de joueur est actuellement implémenté : "+
					  str(possibilites[0])+".\nCe type sera choisi pour noir et blanc.")
				self.noir = possibilites[0](self, "noir", opts)
				self.blanc = possibilites[0](self, "blanc", opts)
			else:
				print("Quel type de joueur souhaitez vous sélectionner pour noir ?",
					  "Les possibilités sont :")
				for i in range(len(possibilites)):
					print(i+1," : "+str(possibilites[i]))
				choix = input("Tapez le nombre correspondant.\n")
				self.noir = possibilites[int(choix)-1](self, "noir", opts)
				print("Quel type de joueur souhaitez vous sélectionner pour blanc ?\nLes possibilités sont :")
				for i in range(len(possibilites)):
					print(i+1," : "+str(possibilites[i]))
				choix = input("Tapez le nombre correspondant.\n")
				self.blanc = possibilites[int(choix)-1](self, "blanc", opts)
		
		if "taille" in opts:
			taille = opts["taille"]
		else:
			taille = 8
		self.plateau = Plateau(taille)
		self.tour = 1
		self.partie_finie = False
		self.joueur_courant = self.noir
		
		if "interface"  not in opts or opts["interface"]:
			self.interface = True
			self.gui = interface.Interface(self)
		else:
			self.interface = False

	def demarrer(self):
		if isinstance(self.joueur_courant, joueur.IA):
			self.gui.active_ia()
		else:
			self.gui.active_humain()
		self.gui.fenetre.mainloop()
		
	def jouer(self, coup, verbose=1):

		if self.tour == 1 and self.joueur_courant == None:
			if self.noir == None:
				raise ValueError("Vous devez initialiser le joueur noir.")
			self.joueur_courant = self.noir
		self.plateau.jouer(coup, self.joueur_courant.couleurval)
		if self.interface:
			self.gui.actualise_plateau()
		
		self.tour += 1
		self.check_partie_finie()
		
		if self.partie_finie:
			m = "Victoire de "+self.vainqueur+"."
			print(m)
			if self.interface:
				self.gui.desactive_humain()
				self.gui.desactive_ia()
				self.gui.message_tour.set("Partie finie.\n"+m)
			self.joueur_courant = None
			
		else:
			if self.tour%2 == 1:
				self.joueur_courant = self.noir
				if self.interface:
					self.gui.message_tour.set("A noir de jouer")
			else:
				self.joueur_courant = self.blanc
				if self.interface:
					self.gui.message_tour.set("A blanc de jouer")

	def check_partie_finie(self):
		for i in range(self.plateau.taille):
			if self.plateau[i,-1] == 1:
				self.partie_finie = True
				self.vainqueur = "noir"
				return True
			if self.plateau[i,0] == -1:
				self.partie_finie = True
				self.vainqueur = "blanc"
				return True
		return False

class Plateau:
	
	def __init__(self, taille=8):
		self.tableau_cases = [[0 for j in range(taille)] for i in range(taille)]
		for i in range(taille):
			self.tableau_cases[i][0] = self.tableau_cases[i][1] = 1
			self.tableau_cases[i][-1] = self.tableau_cases[i][-2] = -1
		self.taille = taille
		
	def jouer(self, coup, couleurval):
		try:
			case_depart, case_arrivee = coup
		except:
			return
			#print(coup)
			#raise Exception(" Pb {}:".format(coup))
		if self.est_coup_valide(coup, couleurval):
			self.tableau_cases[case_arrivee[0]][case_arrivee[1]] = self.tableau_cases[case_depart[0]][case_depart[1]]
			self.tableau_cases[case_depart[0]][case_depart[1]] = 0
		else:
			print(coup)
			raise Exception("Le coup donné n'est pas valide")

	def est_coup_valide(self, coup, couleurval=None):
		depart, arrivee = coup
		if depart[0] < 0 or depart[0] >= self.taille or depart[1] < 0 or depart[1] >= self.taille:
			return False
		if arrivee[0] < 0 or arrivee[0] >= self.taille or arrivee[1] < 0 or arrivee[1] >= self.taille:
			return False
		c_depart = self.tableau_cases[depart[0]][depart[1]]
		if c_depart == 0:
			return False
		if couleurval is not None and c_depart != couleurval:
			return False #pas la bonne couleur
		c_arrivee = self.tableau_cases[arrivee[0]][arrivee[1]]
		if c_depart == c_arrivee:
			return False
		if c_depart == 1:
			sens = 1
		else:
			sens = -1
		if arrivee[1] != depart[1]+sens:
			return False
		if arrivee[0] < depart[0] - 1 or arrivee[0] > depart[0] + 1:
			return False
		return True

	def liste_coups_valides(self, couleurval):
		'''Rend la liste des coups valides pour un joueur de couleur couleurval.'''
		coups_valides = []
		for i in range(self.taille):
			for j in range(self.taille):
				if self.tableau_cases[i][j] == couleurval:
					for k in (-1,0,1):
						if self.est_coup_valide(((i,j), (i+k,j+couleurval)), couleurval):
							coups_valides.append(((i,j),(i+k,j+couleurval)))
		return coups_valides
	
	def copie(self):
		copie = Plateau(self.taille)
		copie.tableau_cases = [[self.tableau_cases[i][j] for j in range(self.taille)] for i in range(self.taille)]
		return copie
	
	def __getitem__(self, coords):
		i, j = coords
		return self.tableau_cases[i][j]




