ROCHETTE Arthur Grp 2

### Partie de M.Chotard (copie aussi dans son depot)

####Arbres de jeu
 
1) noeuds : etats du jeu (marque d'une probabilite de victoire )
aretes : des decisions
feuilles: des etats finaux (victoire, defaite, egalite)

2) Les noeuds ayant un score le plus eleve, ils sont donc les enfants les plus prometteurs 

3) l'utilite d'une heuristique permet de dirigier le developpement de l'arbre, l'heuristique donne un indice sur la qualite du choix effectue.

4) Si l'on considere qu'un affrontement gagne resulte en la disparition de la piece ennemie, et que la victoire et accordee au joueur ayant le plus de piece, alors il faudrait faire une heuristique comptant le nombre de pieces ennemie par rapport au nombre de piece allie.

5) il est plus interressant d'utiliser mcts dans des jeux ou le nombre de possibilite de coup a chaque tour est eleve.

6) mimax va retourner V.

7) MCTS tend vers un jeu optimal.

#### Theorie des jeux

8) Jeu a somme nulle et a information complete 

9) L'equilibre de Nash est quand les deux joueurs ont les memes pertes/gains. (Dilemme du prisonnier)


10) a)
```
                      A
              Marvel     DC         
    Marvel      3        2! 0       

B

    DC          1! 0      4         

```
b) l'equilibre de nash est donc un film DC. 


#### Breakthrough

```python
class MCTS_h(IA):
    class Noeud:
        def __init__(self, plateau,couleurval, parent=None, parent_action=None, heuristic):
            self.plateau = plateau
            self.parent = parent
            self.parent_action = parent_action
            self.enfants = []
            self.couleurval = couleurval
            self._results = defaultdict(int)
            self.win = 0
            self.loses = 0
            self.visits = 0
            self.h = heuristic
            return

        def expand(self):
            meilleur_coup = (-100000, None ) # ( score, coup)
            for coup in self.plateau.list_coups_valides:
                new_plateau = self.plateau.copie()
                new_plateau.joueur(coup, self.couleurval)
                score = self.h(self.couleurval, new_plateau)
                if meilleur_coup[0] < score:
                    meilleur_coup[0] = score
                    meilleur_coup[0] = coup
                    
            coup = meileur_coup[1]
            newplateau = self.plateau.copie()
            newplateau.jouer(coup, self.couleurval)
            noeud_enfant = MCTS.Noeud(newplateau, -self.couleurval, self, coup)
            self.enfants.append(noeud_enfant)
            return noeud_enfant

        def is_end(self):
            return iswin(self.plateau, self.couleurval) or iswin(self.plateau, -self.couleurval)

        def evaluate(self):
            copie = self.plateau.copie()
            localcolor = self.couleurval

            while not (iswin(copie, self.couleurval) or iswin(copie, -self.couleurval)):
                new_coups = copie.liste_coups_valides(localcolor)
                copie.jouer(new_coups[np.random.randint(len(new_coups))], localcolor) 
                localcolor = -localcolor

            return self.couleurval if iswin(copie, self.couleurval) else -self.couleurval

        def backpropagate(self, result):
            self._results[result] += 1
            self.visits += 1
            if self.parent:
                self.parent.backpropagate(result)

        def is_fully_expanded(self):
            return len(self.enfants) == len(self.plateau.liste_coups_valides(self.couleurval))


        def best_child(self, k_param = 0.1):
            weight = (self.win / self.visits) + k_param * np.sqrt(np.log(self.visits) / self.visits)  
            print(len(self.enfants))
            return self.enfants[np.argmax(weight)] 
        def strategie_arbre(self):
            noeud = self
            while not self.is_end():
                if noeud.is_fully_expanded():
                    noeud = noeud.best_child()
                else:
                    return noeud.expand()
            return noeud
        

        def action(self):
            for i in range(400):
                v = self.strategie_arbre()
                reward = v.evaluate()
                v.backpropagate(reward)
            return self.best_child(k_param=0.0)
        
    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts=opts)
        self.last_time = 0

    def demande_coup(self):
        return self.mcts()


    def mcts(self):
        noeud = self.Noeud(self.jeu.plateau, self.couleurval)
        return noeud.action().parent_action


```
